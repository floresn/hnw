/// <binding AfterBuild='default' />
"use strict";

var gulp = require('gulp');
var connect = require('gulp-connect'); // runs a local dev server
var open = require('gulp-open'); // opens a url in web browser
var browserify = require('browserify'); // js bundler
var babelify = require('babelify'); // transforms react jsx to js
var source = require('vinyl-source-stream'); // use conventional text streams with gulp
var concat = require('gulp-concat'); // concatenates files
var lint = require('gulp-eslint'); // link js files, including jsx
var sass = require('gulp-sass'); // sass compiler
var merge = require('merge-stream'); // merges streams
var cleanCss = require('gulp-clean-css'); // minifies css
var envify = require('envify');

//devBaseUrl:'http://localhost',
//devBaseUrl:'http://amp.react.com',
var config = {
    port: 3000,
    devBaseUrl: 'http://amp.react.com',
    paths: {
        html: './src/*.html',
        js: [
            './src/**/*.js'
        ],
        css: [
            'node_modules/toastr/build/toastr.min.css',
            'node_modules/font-awesome/css/font-awesome.min.css'
        ],
        scss: './src/scss/*',
        images: './src/images/*',
        fonts: [
            'node_modules/font-awesome/fonts/*'
        ],
        dist: './dist',
        indexJs: './src/index.js',
        mainScss: './src/scss/main.scss',
        components: './src/components'
    }
};

// start dev server
gulp.task('connect', function () {
    var cors = function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', 'https://definedlogic.auth.us-east-2.amazoncognito.com');
        res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        next();
    };
    connect.server({
        root: ['dist'],
        port: config.port,
        base: config.devBaseUrl,
        livereload: true,
        cors: true,
        fallback: 'dist/index.html',
        middleware: function () { return [cors] }
    });
});

// open index.html
gulp.task('open', ['connect'], function () {
    gulp.src('dist/index.html')
        .pipe(open({ uri: config.devBaseUrl + ':' + config.port + '/' }));
});

// copy html from src to dist and reload dev server
gulp.task('html', function () {
    gulp.src(config.paths.html)
        .pipe(gulp.dest(config.paths.dist))
        .pipe(connect.reload());
});

// browserify and babelify js and reload dev server
gulp.task('js', function () {

    browserify(config.paths.indexJs)
        .transform('babelify')
        .transform('envify', {})
        .bundle()
        .on('error', console.error.bind(console))
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(config.paths.dist + '/scripts'))
        .pipe(connect.reload());
});

// css concat
gulp.task('css', function () {

    var scssStream = gulp.src(config.paths.mainScss)
        .pipe(sass())
        .pipe(cleanCss())
        .pipe(concat('scss-files.scss'))

    var cssStream = gulp.src(config.paths.css)
        .pipe(concat('css-files.css'));

    merge(scssStream, cssStream)
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest(config.paths.dist + '/css'))
        .pipe(connect.reload());
});

// copies fonts to dist folder
gulp.task('fonts', function () {
    gulp.src(config.paths.fonts)
        .pipe(gulp.dest(config.paths.dist + '/fonts'))
        .pipe(connect.reload());
});

// copies images to dist folder
gulp.task('images', function () {
    gulp.src(config.paths.images)
        .pipe(gulp.dest(config.paths.dist + '/images'))
        .pipe(connect.reload());

    // publish favicon
    gulp.src('./src/favicon.ico')
        .pipe(gulp.dest(config.paths.dist));
});

// lint
gulp.task('lint', function () {
    return gulp.src(config.paths.js)
        .pipe(lint({ config: '.eslintrc' }))
        .pipe(lint.format());
    //.pipe(lint.failAfterError());
});

// watch
gulp.task('watch', function () {
    gulp.watch(config.paths.html, ['html']);
    gulp.watch(config.paths.js, ['js', 'lint']);
    gulp.watch([config.paths.scss], ['css']);
});

// default
gulp.task('default', ['html', 'js', 'css', 'images', 'fonts', 'lint']);
//gulp.task('default', ['html', 'js', 'css', 'images', 'fonts', 'lint', 'open', 'watch']);



