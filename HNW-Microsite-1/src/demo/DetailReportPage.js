"use strict";

import React from 'react'
import CoreLayout from '../layouts/CoreLayout'
import PageTitle from '../common/PageTitle'

export default class DetailReportPage extends React.Component {

    render() {
        return ( 
            <CoreLayout>
                <PageTitle title="Detail Report" subtitle="Reports"/>
                <hr/>
            </CoreLayout>
        )
    }
}