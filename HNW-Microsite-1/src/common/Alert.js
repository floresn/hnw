"use strict";

import React from 'react'
import classnames from 'classnames'

export default class Alert extends React.Component {

    render() {
        var className = classnames(this.props.className, 'alert text-center');
        return (<div className={className} role="alert">{this.props.message}</div>);
    }
}