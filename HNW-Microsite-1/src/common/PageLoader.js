"use strict";

import React from 'react'

export default class PageLoader extends React.Component {
    render() {
        return (
            <div className="container h-100">
                <div className="row h-100 justify-content-center align-items-center">
                    <div className="app-loader"></div>
                </div>
            </div>
        );
    }
}