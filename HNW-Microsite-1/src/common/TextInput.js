'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class TextInput extends React.Component {

    render() {

        var { label, error, ...attrs } = this.props; // filter out non-input attributes
        
        var className = classnames(this.props.className, 'form-control');

        if (this.props.error && this.props.error.length > 0) {
            className += ' ' + 'is-invalid form-control-invalid';
        }

        return (
            <div className='form-group'>
                {this.props.label &&
                    <label htmlFor={this.props.name}>{this.props.label}</label>
                }
                <input type={this.props.type || 'text'}
                    {...attrs}
                    className = {className}
                    ref = {this.props.name}/>
                <div className='invalid-feedback'>{this.props.error}</div>
            </div>
        );
    }
}

TextInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    error: PropTypes.string
}

