"use strict";

import React from 'react'
import PropTypes from 'prop-types'

export default class PageTitle extends React.Component 
{
    render() {
        return (
            <div>
                <h6 className="page-subtitle">{this.props.subtitle}</h6>
                <h2 className="page-title">{this.props.title}</h2>
            </div>
        )
    }
}

PageTitle.propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string
}