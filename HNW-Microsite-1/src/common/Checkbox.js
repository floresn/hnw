'use strict';

import React from 'react';
import PropTypes from 'prop-types';

export default class Checkbox extends React.Component {

    render() {

        var { label, error, ...attrs } = this.props; // filter out non-input attributes
        
        var className = this.props.className;

        if (this.props.error && this.props.error.length > 0) {
            className += ' ' + 'is-invalid';
        }

        return (
            <div className='form-group'>
                <label>
                    <input 
                        type='checkbox' 
                        {...attrs} 
                        className = {this.props.className}
                        ref = {this.props.name}/> {this.props.label}
                </label>
                <span className='invalid-feedback'>{this.error}</span>
            </div>
        );
    }
}

Checkbox.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.bool,
    error: PropTypes.string
}

