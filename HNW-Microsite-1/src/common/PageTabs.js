"use strict"

import React from 'react';
import {Nav, NavItem, NavLink} from 'reactstrap';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default class PageTabs extends React.Component
{
    constructor(props)
    {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab:'1'
        };
    }

    toggle(tabId) {
        if (this.state.activeTab !== tabId) {
            this.setState({
                activeTab: tabId
            });
            this.props.onTabChange(tabId);
        }
    }
    
    render()
    {
       var createTab = function(item, index) {

            var tabId = (index + 1).toString();
    
            return (
                <NavItem key={index}>
                    <NavLink
                        className={classnames({ active:this.state.activeTab === tabId})}
                        onClick={() => {this.toggle(tabId)}}>
                        {item}
                    </NavLink>
                </NavItem>
            )
        }

        var className = classnames(this.props.className, 'page-tabs');

        return (
            <Nav pills className={className}>
                {this.props.tabs.map(createTab, this)}
            </Nav>
        )
    }
}