import React from 'react';
import { Authenticator } from 'aws-amplify-react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import routePaths from './utils/routePaths'
import LogoutPage from './auth/LogoutPage'
import LoginPage from './auth/LoginPage';
import { Auth, Cache } from 'aws-amplify';
import awsConfig from './aws-exports';
import TestHomePage from './home/TestHome';

const config = {
    authCode = '',
    tokenUrl: 'https://definedlogic3.auth.us-east-2.amazoncognito.com/oauth2/token?' +
    'grant_type=authorization_code' +
    '&client_id=14b5mi27u27l8ls5baaukjmop5' +
    '&code=' + authCode +
    '&redirect_uri=https://hnw-microsite-1.azurewebsites.net',

    authUrl: 'https://definedlogic3.auth.us-east-2.amazoncognito.com/oauth2/authorize?' +
    '&response_type=CODE' +
    '&client_id=14b5mi27u27l8ls5baaukjmop5' +
    '&redirect_uri=https://hnw-microsite-1.azurewebsites.net' +
    '&identity_provider=DL-Azure' +
    '&scope=openid+email',

    federatedUrl: 'cognito-idp.us-east-2.amazonaws.com/us-east-2_hoowqn4zw'
}

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: false,
            loading: false
        }

        this.handleAuthStateChanged = this.handleAuthStateChanged.bind(this);
    }

    componentDidMount() {
        // Checking user authentication
        Auth.currentAuthenticatedUser()
            .then((result) => {
                this.authenticateUser();
            })
            .catch((error) => {
                //check if auth code exists
                authCode = this.getQueryValue('code');
                if (authCode) {
                    // api call to authorize user
                    fetch(config.tokenUrl, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                    })
                        .then(response => response.json())
                        .catch(error => { console.log('error occured: ' + error) })
                        .then((tokens) => {
                            // cache tokens
                            Cache.setItem('authTokens', tokens);

                            this.authenticateUser();
                        })
                        .catch(error => { console.log('error occured: ' + error) });
                } else {
                    // authorize federated user
                    window.location.href = config.authUrl;
                }
            });
    }

    authenticateUser() {
        var tokens = Cache.getItem('authTokens');

        // signin with federated info
        var user = {};
        Auth.federatedSignIn(config.federatedUrl, {
            token: tokens.id_token,
            expires_at: tokens.expires_at
        }, user)
            .then(user => {
                // cache user
                Cache.setItem('user', user);
            })
            .catch((error) => console.log('Federated signin error: ' + error));
    }

    getQueryValue(variable) {
        var query = window.location.search.substring(1);
        var variables = query.split('&');
        for (var i = 0; i < variables.length; i++) {
            var pair = variables[i].split('=');
            if (decodeURIComponent(pair[0]) == variable)
                return decodeURIComponent(pair[1]);
        }

        console.log('Query variable %s not found', variable);
    }

    handleAuthStateChanged(state) {
        if (state == 'signedIn')
            this.setState({ authenticated: true });
        else
            this.setState({ authenticated: false });
    }

    render() {
        if (!this.state.authenticated) {
            return (
                <Authenticator hideDefault={true} onStateChange={this.handleAuthStateChanged}>
                    <LoginPage />
                </Authenticator>
            );
        }
        else {
            return (
                <Authenticator hideDefault={true} onStateChange={this.handleAuthStateChanged}>
                    <Router>
                        <Switch>
                            <Route path={routePaths.ROOT} component={TestHomePage} exact />
                        </Switch>
                    </Router>
                </Authenticator>
            );
        }
    }
}