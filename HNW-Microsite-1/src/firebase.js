import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyDSfUgG0dC7K0cXVa8XisR9457HiPv_DI4",
    authDomain: "react-app-b4cce.firebaseapp.com",
    databaseURL: "https://react-app-b4cce.firebaseio.com",
    projectId: "react-app-b4cce",
    storageBucket: "react-app-b4cce.appspot.com",
    messagingSenderId: "611889751310"
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
  
  export const provider = new firebase.auth.GoogleAuthProvider();
  export const auth = firebase.auth();
  
  export default firebase;