'use strict';

import React from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import Link from 'react-router-dom'
import logger from '../utils/logger'

export default class CoreLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = { hasError: false };
        this.renderError = this.renderError.bind(this);
    }

    componentDidCatch(error, info) { // only handles render errors, not errors from event handlers

        logger.logError(error);
        this.setState({ hasError: true });
    }

    render() {

        return (
            <div className="core-layout">
                <Header />
                <div className="container-fluid">
                <div className="row flex-xl-nowrap">
                <div className="col-12 col-md-4 col-xl-3">
                    <Sidebar/>
                </div>
                <main className="col-12 col-md-8 col-xl-9 py-md-3 pl-md-5">
                    {this.state.hasError ? this.renderError() : this.props.children}
                </main>
                </div>
                </div>
            </div>
        );
    }

    renderError() {
        return (
            <div>
                <h1>Sorry, something went wrong.</h1>
                <p>Unfortunately, the server encountered an error and could not complete your request.</p>
                <p><Link to="/">Back to Home</Link></p>
            </div>             
        );
    }
}