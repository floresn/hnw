"use strict";

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Navbar, Nav, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarBrand } from 'reactstrap'
import translate from '../i18n/translate'
import AuthStore from '../auth/AuthStore'
import routePaths from '../utils/routePaths'

export default class Header extends React.Component {

    constructor(props) {

        super(props);
        this.state = { user: {} };
    }

    componentDidMount() {
        console.log('hitting auth store to get user')
        var user = AuthStore.getUser();
        console.log('user: ' + user);
        this.setState({ user: user });
    }

    render() {

        return (
            <Navbar expand="xs" dark>
                <NavbarBrand href="/">
                    <span className="d-md-none">dl</span>
                    <span className="d-none d-md-inline">definedlogic</span>
                </NavbarBrand>
                <Nav className="ml-auto" navbar>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            <span className="fa fa-user fa-fw" />
                            <span className="d-none d-md-inline">{this.state.user.displayName}</span>
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem href="#">
                                {translate('header.profile')}
                            </DropdownItem>
                            <DropdownItem href="#" >
                                Another Link
                        </DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem href={routePaths.LOGOUT}>
                                {translate('header.logOut')}
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </Navbar>
        );
    }
}