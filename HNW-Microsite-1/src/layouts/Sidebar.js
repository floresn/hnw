
import React from 'react'
import { Navbar, NavbarToggler, Nav, NavItem, Collapse } from 'reactstrap';
import { NavLink } from 'react-router-dom';

export default class Sidebar extends React.Component {

    constructor(props) {
        super(props);
        this.toggleOpen = this.toggleOpen.bind(this);
        this.state = { isOpen: false };
    }
    
    toggleOpen(event) {
        event.preventDefault();
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        return (
            <div className="sb">
                <form className="d-flex align-items-center sb-search">
                <button onClick={this.toggleOpen} className="btn btn-link sb-toggle-btn d-md-none p-0 mr-3 collapsed">
                    <span className="fa fa-lg fa-bars"/>
                </button>
                <input type="text" className="sb-search-input form-control" placeholder="Search ..." aria-label="Search"/>
                <button className="btn sb-search-btn" type="button">
                    <span className="fa fa-search"/>
                </button>
                </form>
                <Collapse isOpen={this.state.isOpen} className="sb-links">
                    <Nav vertical pills>
                        <NavItem className="sb-nav-title">
                            Reports
                        </NavItem>
                        <NavItem>
                            <NavLink to="/" className="nav-link" activeClassName="active" exact>Overview</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink to="/detail-report" className="nav-link" activeClassName="active" exact>Detail Report</NavLink>
                        </NavItem>
                        <NavItem className="sb-nav-title">
                            More
                        </NavItem>
                        <NavItem>
                            <NavLink to="/tools" className="nav-link" activeClassName="active" exact>Tools</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
                <hr/>
            </div>
        );
    }
} 
