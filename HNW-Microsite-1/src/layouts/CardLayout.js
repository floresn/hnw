
import React from 'react'
import routePaths from '../utils/routePaths'
import Link from 'react-router-dom'
import logger from '../utils/logger'

export default class CardLayout extends React.Component {

    constructor(props) {
        super(props);
        this.state = { hasError: false };
        this.renderError = this.renderError.bind(this);
    }

    componentDidCatch(error, info) { // only handles render errors, not errors from event handlers

        logger.logError(error);
        this.setState({ hasError: true });
    }

    render() {
        return (
            <div className="container h-100">
                <div className="row h-100 justify-content-center align-items-center">
                    <div className="login">
                        <div className="card">
                            <div className="card-body">
                                <div className="login-brand text-center"><a href={routePaths.ROOT}><img src="../../dist/images/dl-logo.png" /></a></div>
                                {this.state.hasError ? this.renderError() : this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderError() {
        return (
            <div>
                <h1>Sorry, something went wrong.</h1>
                <p>Unfortunately, the server encountered an error and could not complete your request.</p>
                <p><Link to="/">Back to Home</Link></p>
            </div>
        );
    }
}