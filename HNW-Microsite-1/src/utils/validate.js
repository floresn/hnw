
import validate from 'validate.js';
import _ from 'lodash';

// customizations
validate.formatters.custom = function(errors) { 

    if (errors) {

        var result = {};
        var groups = validate.groupErrorsByAttribute(errors);

        _.keys(groups).forEach(function(key) {
            result[key] = groups[key][0].error; // get only first error
        });

        return result;
    }
    else {
        return undefined;
    }
}

validate.options = {format: 'custom', fullMessages:false};
validate.async.options = {format: 'custom', fullMessages:false};
validate.validators.email.message = 'Please enter a valid email address';
validate.validators.presence.message = "Required";

// export validate.js w/ customizations
export default validate;