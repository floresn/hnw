
export default {
    ROOT:'/',
    LOGIN:'/login',
    LOGOUT:'/logout',
    FORGOT_PASSWORD: '/password-assistance',
    RESET_PASSWORD: '/password-reset',
    RESET_PASSWORD_SUCCESS: '/password-reset/success'
}