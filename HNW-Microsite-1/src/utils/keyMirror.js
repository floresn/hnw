import _ from 'lodash';

export default function keyMirror(keys) {
    keys = Array.isArray(keys) ? keys : Object.keys(keys);
    return _.reduce(keys, function(res, v) {
        res[v] = v;
        return res;
    }, {});
}