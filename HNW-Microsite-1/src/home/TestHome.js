﻿import React from 'react';
import { Auth, API, Cache, Hub, Storage } from 'aws-amplify';

const config = {
    logOutUrl: 'https://definedlogic3.auth.us-east-2.amazoncognito.com/logout?' +
    '&client_id=14b5mi27u27l8ls5baaukjmop5' +
    '&logout_uri=https://hnw-microsite-1.azurewebsites.net',
};

export default class TestHome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    logout() {
        Auth.signOut()
            .then((data) => {
                window.location.href = config.logOutUrl
            })
            .catch(error => console.log('error: ' + error));
    }

    render() {
        return (
            <div>
                <h1>Welcome home!</h1>
                <div>
                    <a href='https://hnw-portal.azurewebsites.net'>Go to HNW PORTAL</a>
                </div>
                <div>
                    <button
                        className="btn btn-lg btn-primary btn-block"
                        type="submit"
                        onClick={this.logout}
                        tabIndex="3">Log OUT</button>
                </div>
            </div>
        );
    }
}