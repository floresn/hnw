"use strict";

import React from 'react'
import CoreLayout from '../layouts/CoreLayout'
import PageTitle from '../common/PageTitle'
import PageTabs from '../common/PageTabs'

export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.toggleContent = this.toggleContent.bind(this);
    }

    toggleContent(tabId) {
        
    }

    render() {

        var tabs = [
            'Traffic',
            'Sales',
            'Support'
        ];

        return (
            <CoreLayout>
                <PageTitle title="Overview" subtitle="Reports"/>
                <PageTabs tabs={tabs} onTabChange={this.toggleContent}/>
                <hr/>
            </CoreLayout>
        );
    }
}
