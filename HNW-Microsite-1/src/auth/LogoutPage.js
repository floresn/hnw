'use strict';

import React from 'react'
import firebase from '../firebase'
import {Redirect} from 'react-router-dom'
import routePaths from '../utils/routePaths'

export default class LogoutPage extends React.Component {

    render() {

        try {
            firebase.auth().signOut();
        }
        catch(error) {

        }

        return <Redirect to={routePaths.LOGIN}/>;
    }
}