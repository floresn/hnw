"use strict";

import React from 'react'
import { Route, Redirect } from "react-router-dom"
import AuthStore from '../auth/AuthStore'
import PageLoader from '../common/PageLoader'
import firebase from '../firebase'
import AuthActions from '../auth/AuthActions'

export default class ProtectedRoute extends React.Component { 

  constructor(props) {
    super(props);
    this.state = {loading:true};
  }

  componentDidMount () {

      this.removeAuthStateListener = firebase.auth().onAuthStateChanged((fbUser) => {

          AuthActions.setAuth(fbUser);

          this.setState({loading:false});
      })
  }

  componentWillUnmount () {
      this.removeAuthStateListener()
  }

  render() {

      var {component: Component, ...rest} = this.props;

      return (
          this.state.loading === true 
          ? <PageLoader/> 
          : <Route
                {...rest}
                render={props =>
                  AuthStore.isAuthenticated() && (!props.permission || AuthStore.isAuthorized(props.permission)) ? (
                    <Component {...props} />
                  ) : (
                    <Redirect
                      to={{
                        pathname: "/login",
                        state: { from: props.location }
                      }}
                    />
                  )
                }
              />
      )
  }
}

// export default  ({ component: Component, ...rest }) => (
//     <Route
//       {...rest}
//       render={props =>
//         AuthStore.isAuthenticated() && (!props.permission || AuthStore.isAuthorized(props.permission)) ? (
//           <Component {...props} />
//         ) : (
//           <Redirect
//             to={{
//               pathname: "/login",
//               state: { from: props.location }
//             }}
//           />
//         )
//       }
//     />
//   );