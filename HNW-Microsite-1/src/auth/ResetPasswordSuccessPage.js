import React from 'react'
import translate from '../i18n/translate'
import routePaths from '../utils/routePaths'
import Alert from '../common/Alert'

export default class ResetPasswordSuccessPage extends React.Component {

    render() {
        return(
            <CardLayout>
                <Alert className="alert-success mt-3" message={translate('resetPassword.successMessage')}/>
                <div className="mt-3 text-center">
                    <a href={routePaths.LOGIN}>{translate('resetPassword.goToLogin')}</a>
                </div>
            </CardLayout>
        );
    }
}
