'use strict';

import React from 'react'
import {Redirect} from 'react-router'
import qs from 'query-string'
import routePaths from '../utils/routePaths'

// gateway for firebase auth requests (reset password, confirm email, etc.)
export default class FirebaseAuthActionRouter extends React.Component {

    render() {

        var mode, code;
        var params = qs.parse(this.props.location.search) || {};
        
        if (params) {
            mode = params.mode;
            code = params.oobCode;
        }

        switch(mode) {
            case 'resetPassword':
                return (<Redirect to={routePaths.RESET_PASSWORD + '?code=' + code} />);
            default:
                // return page not found
                throw 'not implemented';
        }
    }
}