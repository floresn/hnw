"use strict";

import React from 'react'
import translate from '../i18n/translate'
import routePaths from '../utils/routePaths'
import Alert from '../common/Alert'

export default class ResetPasswordFailure extends React.Component {

    render() {
        return (
            <div>
                <Alert className="alert-invalid mt-3" message={this.props.message}/>
                <div className="mt-3 text-center">
                    <div><a href={routePaths.FORGOT_PASSWORD}>{translate('resetPassword.passwordAssistance')}</a></div>
                    <div><a href={routePaths.LOGIN}>{translate('resetPassword.login')}</a></div>
                </div>
            </div>
        );
    }
}