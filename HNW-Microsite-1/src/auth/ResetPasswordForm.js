
import React from 'react'
import PropTypes from 'prop-types'
import validate from '../utils/validate'
import translate from '../i18n/translate'
import TextInput from '../common/TextInput'

const validationRules = {
    password: {
        presence:{allowEmpty:false}
    },
    confirmPassword: {
        presence:{allowEmpty:false},
        equality:"password"
    }
}

export default class ResetPasswordForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {input:{password:'', confirmPassword:''}, errors:{}};
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event) {

        event.preventDefault();

        var errors = validate(this.state.input, validationRules);

        this.setState({errors:errors || {}});

        if (errors) {
            return;
        }

        this.props.onSubmit(this.state.input.password);
    }

    onInputChange(event) {

        var input = this.state.input;
        input[event.target.name] = event.target.value;
        this.setState({input:input});
    }

    render() {
        return (
            <div>
            <p className="text-center">{translate('resetPassword.instructions')}</p>
            {this.props.errorMessage && this.props.errorMessage.length > 0 && 
                <div className="alert alert-danger text-center">{this.props.errorMessage}</div>
            }
            <form role="form" autoComplete="off">
                <TextInput
                    id="inputPassword"
                    name="password"
                    type="password"
                    placeholder={translate('resetPassword.newPassword')}
                    onChange={this.onInputChange}
                    error={this.state.errors.password}
                    autoFocus 
                    tabIndex="1"/> 
                <TextInput
                    id="inputConfirmPassword"
                    name="confirmPassword"
                    type="password"
                    placeholder={translate('resetPassword.confirmNewPassword')}
                    onChange={this.onInputChange}
                    error={this.state.errors.confirmPassword}
                    autoFocus 
                    tabIndex="1"/> 
                <button 
                    className="btn btn-lg btn-primary btn-block" 
                    type="submit" 
                    onClick={this.onSubmit} 
                    tabIndex="2">{translate('resetPassword.save')}</button>
            </form>
            </div>
        );
    }

}

ResetPasswordForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    errorMessage: PropTypes.string
}