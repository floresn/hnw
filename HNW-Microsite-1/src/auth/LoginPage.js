"use strict";

import React from 'react'
import CardLayout from '../layouts/CardLayout'
import validate from '../utils/validate'
import TextInput from '../common/TextInput'
import Checkbox from '../common/Checkbox'
import { Auth } from 'aws-amplify';
import translate from '../i18n/translate'
import routePaths from '../utils/routePaths'

const validationRules = {
    email: {
        presence: { allowEmpty: false }
    },
    password: {
        presence: { allowEmpty: false }
    }
}

const config = {
    url: 'https://definedlogic3.auth.us-east-2.amazoncognito.com/oauth2/authorize?' +
    '&response_type=CODE' +
    '&client_id=14b5mi27u27l8ls5baaukjmop5' +
    '&redirect_uri=https://hnw-microsite-1.azurewebsites.net' +
    '&identity_provider=DL-Azure' +
    '&scope=openid+email'
}

export default class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: { email: '', password: '', rememberMe: false },
            errors: {}
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onLogIn = this.onLogIn.bind(this);
    }

    onLogIn(event) {

        event.preventDefault();

        var errors = validate(this.state.login, validationRules);

        var errorState = Object.assign({}, errors);

        this.setState({ errors: errorState });

        if (errors) {
            return;
        }

        Auth.signIn(this.state.login.email, this.state.login.password)
            .then((user) => {
                console.log(JSON.stringify(user));
            })
            .catch((error) => { alert('We failed loggin in: ' + JSON.stringify(error)) })
    }

    onInputChange(event) {

        var name = event.target.name;
        var value = event.target.value;
        var login = Object.assign({}, this.state.login);

        login[name] = value;

        this.setState({ login: login });
    }

    render() {
        return (
            <CardLayout>
                {this.state.errorMessage && this.state.errorMessage.length > 0 &&
                    <div className="alert alert-danger">{this.state.errorMessage}</div>
                }
                <form role="form" autoComplete="off">
                    <TextInput
                        id="inputEmail"
                        name="email"
                        placeholder={translate('login.emailAddress')}
                        onChange={this.onInputChange}
                        error={this.state.errors.email}
                        autoFocus
                        tabIndex="1" />
                    <div className="text-right mb-1">
                        <a href={routePaths.FORGOT_PASSWORD}>{translate('login.forgotPassword')}</a>
                    </div>
                    <TextInput
                        id="inputPassword"
                        name="password"
                        type="password"
                        placeholder={translate('login.password')}
                        onChange={this.onInputChange}
                        error={this.state.errors.password}
                        tabIndex="2" />
                    <Checkbox
                        id="inputRememberMe"
                        name="rememberMe"
                        label={translate('login.rememberMe')}
                        value={this.state.login.rememberMe}
                        onChange={this.onInputChange} />
                    <button
                        className="btn btn-lg btn-primary btn-block"
                        type="submit"
                        onClick={this.onLogIn}
                        tabIndex="3">Log In</button>
                    <div>
                        <a href={config.url}>Federated Login</a>
                    </div>
                </form>
                <div className="mt-3 text-center">
                    {translate('login.noAccount')} <a href="#">{translate('login.register')}</a>
                </div>
            </CardLayout>
        );
    }
}