"use strict";

import AppDispatcher from '../app/AppDispatcher';
import AuthActionTypes from './AuthActionTypes';
import {EventEmitter} from 'events';
import _ from 'lodash';

const CHANGE_EVENT = 'change';

var _user;

var AuthStore = Object.assign({}, EventEmitter.prototype, { // copy members of EventEmitter and custom members to new object

    getUser: function() {
        return _user;
    },
    isAuthenticated: function() {
        return Boolean(_user);
    },
    isAuthorized: function(permission) {
        throw 'not implemented';
    },
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },
});

AppDispatcher.register(function(action) {
    switch(action.actionType) {
        case AuthActionTypes.AUTH_UPDATE:
            _user = action.user;
            AuthStore.emitChange();
            break;;
    }
});

export default AuthStore;