"use strict";

import keyMirror from '../utils/keyMirror';

export default keyMirror({
    AUTH_UPDATE: null
});