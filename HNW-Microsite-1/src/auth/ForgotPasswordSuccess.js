import React from 'react'
import translate from '../i18n/translate'

export default class ForgotPasswordSuccess extends React.Component {

    render() {
        return(
            <div className="alert alert-success text-center mt-3" role="alert">{translate('forgotPassword.confirmationMessage')}</div>
        );
    }
}
