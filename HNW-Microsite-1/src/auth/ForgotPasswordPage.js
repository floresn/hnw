"use strict";

import React from 'react'
import firebase from '../firebase'
import CardLayout from '../layouts/CardLayout'
import ForgotPasswordForm from './ForgotPasswordForm'
import Alert from '../common/Alert'
import translate from '../i18n/translate'
import routePaths from '../utils/routePaths'
import logger from '../utils/logger'

export default class ForgotPasswordPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isComplete:false}
        this.handleSubmission = this.handleSubmission.bind(this);
        this.handleSubmissionSuccess = this.handleSubmissionSuccess.bind(this);
        this.handleSubmissionError = this.handleSubmissionError.bind(this);
    }

    handleSubmission(email) {

        try {
            firebase.auth().sendPasswordResetEmail(email)
                .then(this.handleSubmissionSuccess)
                .catch(this.handleSubmissionError); 
            }   
        catch (error) {
            this.handleSubmissionError(error);
        }
    }

    handleSubmissionSuccess() {
        this.setState({isComplete:true});
    }

    handleSubmissionError(error) {

        switch(error.code) {
            case 'auth/user-not-found':
                this.setState({isComplete:true}); // do not inform user if account was not found in case a hack
                break;
            default:
                this.setState({errorMessage:translate('common.unexpectedError')});
                logger.logError(error);
                break;
        }
    }

    render() {
        return (
            <CardLayout>
                <h4 className="text-center">{translate('forgotPassword.title')}</h4>
                {this.state.isComplete === true
                ? <Alert className="alert-success mt-3" message={translate('forgotPassword.successMessage')}/>
                : <ForgotPasswordForm onSubmit={this.handleSubmission} errorMessage={this.state.errorMessage}/>}
                <div className="mt-3 text-center">
                    <a href={routePaths.LOGIN}>{translate('forgotPassword.backToLogin')}</a>
                </div>
            </CardLayout>
        );
    }
}