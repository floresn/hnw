
import React from 'react'
import PropTypes from 'prop-types'
import validate from '../utils/validate'
import translate from '../i18n/translate'
import TextInput from '../common/TextInput'

const validationRules = {
    email: {
        presence:{allowEmpty:false},
        email:true
    }
}

export default class ForgotPasswordForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {input:{email:''}, errors:{}};
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event) {

        event.preventDefault();

        var errors = validate(this.state.input, validationRules);

        this.setState({errors:errors || {}});

        if (errors) {
            return;
        }

        this.props.onSubmit(this.state.input.email);
    }

    onInputChange(event) {

        var input = this.state.input;
        input[event.target.name] = event.target.value;
        this.setState({input:input});
    }

    render() {
        return (
            <div>
            <p className="text-center">{translate('forgotPassword.instructions')}</p>
            {this.props.errorMessage && this.props.errorMessage.length > 0 && 
                <div className="alert alert-danger text-center">{this.props.errorMessage}</div>
            }
            <form role="form" autoComplete="off">
                <TextInput
                    id="inputEmail"
                    name="email"
                    placeholder={translate('forgotPassword.emailAddress')}
                    onChange={this.onInputChange}
                    error={this.state.errors.email}
                    autoFocus 
                    tabIndex="1"/> 
                <button 
                    className="btn btn-lg btn-primary btn-block" 
                    type="submit" 
                    onClick={this.onSubmit} 
                    tabIndex="2">{translate('forgotPassword.sendEmail')}</button>
            </form>
            </div>
        );
    }

}

ForgotPasswordForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    errorMessage: PropTypes.string
}