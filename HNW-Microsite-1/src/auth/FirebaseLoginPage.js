"use strict";

import React from 'react'
import CardLayout from '../layouts/CardLayout'
import validate from '../utils/validate'
import TextInput from '../common/TextInput'
import Checkbox from '../common/Checkbox'
import firebase from '../firebase'
import translate from '../i18n/translate'
import routePaths from '../utils/routePaths'

const validationRules = {
    email: {
        presence: { allowEmpty: false }
    },
    password: {
        presence: { allowEmpty: false }
    }
}

export default class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: { email: '', password: '', rememberMe: false },
            errors: {}
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onLogIn = this.onLogIn.bind(this);
        this.onFederatedLogin = this.onFederatedLogin.bind(this);
    }

    onLogIn(event) {

        event.preventDefault();

        var errors = validate(this.state.login, validationRules);

        var errorState = Object.assign({}, errors);

        this.setState({ errors: errorState });

        if (errors) {
            return;
        }

        firebase.auth().signInWithEmailAndPassword(this.state.login.email, this.state.login.password)
            .then(() => this.props.history.push(routePaths.ROOT))
            .catch((error) => this.setState({ errorMessage: translate('login.invalidLogin') }));
    }

    onInputChange(event) {

        var name = event.target.name;
        var value = event.target.value;
        var login = Object.assign({}, this.state.login);

        login[name] = value;

        this.setState({ login: login });
    }

    render() {
        return (
            <CardLayout>
                {this.state.errorMessage && this.state.errorMessage.length > 0 &&
                    <div className="alert alert-danger">{this.state.errorMessage}</div>
                }
                <form role="form" autoComplete="off">
                    <TextInput
                        id="inputEmail"
                        name="email"
                        placeholder={translate('login.emailAddress')}
                        onChange={this.onInputChange}
                        error={this.state.errors.email}
                        autoFocus
                        tabIndex="1" />
                    <div className="text-right mb-1">
                        <a href={routePaths.FORGOT_PASSWORD}>{translate('login.forgotPassword')}</a>
                    </div>
                    <TextInput
                        id="inputPassword"
                        name="password"
                        type="password"
                        placeholder={translate('login.password')}
                        onChange={this.onInputChange}
                        error={this.state.errors.password}
                        tabIndex="2" />
                    <Checkbox
                        id="inputRememberMe"
                        name="rememberMe"
                        label={translate('login.rememberMe')}
                        value={this.state.login.rememberMe}
                        onChange={this.onInputChange} />
                    <button
                        className="btn btn-lg btn-primary btn-block"
                        type="submit"
                        onClick={this.onLogIn}
                        tabIndex="3">Log In</button>
                </form>
                <div className="mt-3 text-center">
                    {translate('login.noAccount')} <a href="#">{translate('login.register')}</a>
                </div>
            </CardLayout>
        );
    }
}