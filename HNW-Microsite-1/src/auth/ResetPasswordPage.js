"use strict";

import React from 'react'
import firebase from '../firebase'
import CardLayout from '../layouts/CardLayout'
import PageLoader from '../common/PageLoader'
import ResetPasswordForm from './ResetPasswordForm'
import ResetPasswordFailure from './ResetPasswordFailure'
import translate from '../i18n/translate'
import routePaths from '../utils/routePaths'
import qs from 'query-string'
import logger from '../utils/logger'

export default class ResetPassword extends React.Component {

    constructor(props) {

        super(props);
        this.state = {isLoading:true, code:'', failureMessage:'', errorMessage:''};
        this.handleReset = this.handleReset.bind(this);
        this.handleResetError = this.handleResetError.bind(this);
        this.handleResetSuccess = this.handleResetSuccess.bind(this);
        this.handleVerifyError = this.handleVerifyError.bind(this);
    }

    componentWillMount() {

        var params = qs.parse(this.props.location.search) || {};
    
        var code = params ? params.code : '';

        if (!code || code.length == 0) {
            this.setState({isLoading:false, isFailure:true});
            return;
        }

        try {
            firebase.auth().verifyPasswordResetCode(code)
                .then(() => this.setState({isLoading:false, code:code}))
                .catch(this.handleVerifyError);
        }
        catch (error) {
            this.handleVerifyError(error);
        }
    }

    handleReset(password) {

        // save password to firebase
        try {
        firebase.auth().confirmPasswordReset(this.state.code, password)
            .then(this.handleResetSuccess)
            .catch(this.handleResetError);
        }
        catch (error) {
            this.handleResetError(error);
        }
    }

    handleVerifyError(error) {
        
        var failureMessage = '';

        switch (error.code) {
            case 'auth/invalid-action-code':
                failureMessage = translate('resetPassword.invalidCode');
                break;
            default:
                failureMessage = translate('common.unexpectedError');
                logger.logError(error);
                break;
        }

        this.setState({isLoading:false, failureMessage:failureMessage});
    }

    handleResetError(error) {
        
        var failureMessage = '';
        var errorMessage = '';

        switch (error.code) {
            case 'auth/weak-password':
                errorMessage = error.message;
                break;
            case 'auth/invalid-action-code':
                failureMessage = translate('resetPassword.invalidCode');
                break;
            default:
                errorMessage = translate('common.unexpectedError');
                logger.logError(error);
                break;
        }

        this.setState({failureMessage:failureMessage, errorMessage:errorMessage});
    }

    handleResetSuccess() {
        this.props.history.push(routePaths.RESET_PASSWORD_SUCCESS);
    }

    render() {

        if (this.state.isLoading) {
            return <PageLoader/>
        }

        return (
            <CardLayout>
                <h4 className="text-center">{translate('resetPassword.title')}</h4>
                {this.state.failureMessage.length > 0
                    ? <ResetPasswordFailure message={this.state.failureMessage}/>
                    : <ResetPasswordForm onSubmit={this.handleReset} errorMessage={this.state.errorMessage} />}
            </CardLayout>
        );
    }
}