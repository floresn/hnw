"use strict";

import AppDispatcher from '../app/AppDispatcher';
import AuthActionTypes from './AuthActionTypes';
import firebase from '../firebase';

const USER_STORAGE_KEY = "user";

function toLocalUser(fbUser) {
    return {
        email: fbUser.email,
        displayName: fbUser.displayName || fbUser.email
    };
}

export default {
    setAuth: function(fbUser) {

        if (fbUser) {
            AppDispatcher.dispatch({
                actionType: AuthActionTypes.AUTH_UPDATE,
                user:toLocalUser(fbUser)
            });
        }
    }
}