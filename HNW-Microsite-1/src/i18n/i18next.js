"use strict"

import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import en from './translations/en';
import es from './translations/es';

export default i18next
  .use(LanguageDetector)
  .init({
    interpolation: {
      // React already does escaping
      escapeValue: false,
    },
    fallbackLng: 'en',
    resources: {
      en: {
        translation: en
      },
      es: {
        translation: es
      },
    },
    // detection: {
    //   order: ['querystring', 'cookie', 'localStorage', 'navigator', 'htmlTag'],
    // }
  })
