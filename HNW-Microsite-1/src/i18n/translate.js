"use string";

import i18next from './i18next';

export default function(key) {
    return i18next.t(key);
}