export default {
    common: {
        unexpectedError: "Sorry, an unexpected error occurred and your request could not be completed."
    },
    header: {
        profile: 'Profile',
        logOut: 'Log Out'
    },
    login: {
        emailAddress:'Email address',
        password:'Password',
        forgotPassword: 'Forgot Password?',
        rememberMe: 'Remember Me',
        noAccount:'Don\'t have an account?',
        register: 'Register',
        invalidLogin: 'Invalid username/password'
    },
    forgotPassword: {
        title: 'Password Assistance',
        instructions: 'Please enter your email address. We will send you an email to reset your password.',
        emailAddress:'Email address',
        successMessage: 'Please check your email. We\'ve sent an email to reset your password.',
        sendEmail: 'Send Reset Email',
        backToLogin: 'Back to Login'
    },
    resetPassword: {
        title: 'Reset Password',
        instructions: 'Please enter your new password below.',
        newPassword:'New Password',
        confirmNewPassword:'Confirm New Password',
        successMessage: 'Your password has been successfully updated.',
        save: 'Save New Password',
        goToLogin: 'Go to Login',
        login:'Login',
        invalidCode: 'We\'re sorry, the code you provided is invalid or has expired. If you still need assistance, please submit a new request.',
        passwordAssistance: 'Password Assistance'
    }
}