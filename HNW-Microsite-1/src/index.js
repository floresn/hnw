"use strict";

import React from 'react'
import ReactDOM from 'react-dom'

import Amplify from 'aws-amplify';
import awsConfig from './aws-exports';
import AppPage from './AppPage';

Amplify.configure(awsConfig);

ReactDOM.render(
    <AppPage />
    , document.getElementById('app')
)